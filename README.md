# SoLive

Bonjour a vous, 
Comme convenu j'ai pris 4h pour essayé de finir ce test, ce que j'ai pas pu faire malheureusement.
Néanmoins je vous fourni ce que j'ai pu faire.


## User Story	

- En tant que utilisateur je souhaite avoir un formulaire me permettant de créer une équipe de mon choix en précisant : nom et sport (fait)  
- En tant que utilisateur je souhaite avoir un formulaire me permettant de créer un joueur de mon choix en précisant : nom, prénom et sport (fait)  
- En tant que utilisateur je souhaite avoir une interface me permettant d'associer un joueur à une équipe pour une certaine saison  
- Bonus: En tant que utilisateur je souhaite que mes données soient enregistrés afin de garder mes attributions réalisés

## Stack Technique 
- React + Redux  
- NodeJs
- Material UI
## Lancement 
- npm install
- npm start

## Mes Problématiques

Étant donnée que je n'avais fait que très très peu de redux a mon ancien poste, j'ai perdu beaucoup de temps a essayé de comprendre comment ça marche et j'ai toujours pas trop compris (en terme de syntaxe) son fonctionnement, donc malheureusement j'ai pu crée les deux formulaire ainsi que les listes qui contienne les joueurs et équipe et qui pour le coup resteront vide vu que ma compréhension de redux est assez limité 