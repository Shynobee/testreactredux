import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandMore from '@material-ui/core/Icon';
import ExpandLess from '@material-ui/core/Icon';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
});

class PlayerList extends React.Component {
    state = {
        openList: false,
        openTeam: false,
    };

    handleClickList = () => {
        this.setState(state => ({ openList: !state.openList }));
    };

    handleClickTeam = () => {
        this.setState(state => ({ openTeam: !state.openTeam }));
    };

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.root}>
                <List
                    component="nav"
                    subheader={<ListSubheader component="div">Noms D'équipe a mettre</ListSubheader>}
                >
                    <ListItem button onClick={this.handleClickList}>
                        <ListItemText inset primary="Teams" />
                        {this.state.openList ? <ExpandLess /> : <ExpandMore />}
                    </ListItem>
                    <Collapse
                        in={this.state.openList}
                        timeout="auto"
                        unmountOnExit
                    >
                        <List component="div" disablePadding>
                            <ListItem button onClick={this.handleClickTeam}>
                                <ListItemText inset primary="Teams" />
                                {this.state.openTeam ? <ExpandLess /> : <ExpandMore />}
                            </ListItem>
                            <Collapse
                                in={this.state.openTeam}
                                timeout="auto"
                                unmountOnExit
                            >
                                <ListItemText inset primary="wesh morray" />
                            </Collapse>
                        </List>
                    </Collapse>
                </List>
            </div>
        );
    }
}

PlayerList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PlayerList);