import React, { Component } from 'react';
import PlayerForm from './Forms/PlayerForm/PlayerForm';
import TeamForm from './Forms/TeamForm/TeamForm';
import PlayerList from './Lists/PlayerList/PlayerList'
import TeamListWithPlayers from './Lists/TeamsListWithPlayers/TeamsListWithPlayers'

class TeamOrg extends Component {
  render() {
    return (
      <div className="root">
        <div className="formContainer">
          <PlayerForm />
          <TeamForm />
        </div>
        <div className="TeamsListContainer">
          <TeamListWithPlayers />
        </div>
        <div className="PlayersListContainer">
          <PlayerList />
        </div>
      </div>
    );
  }
}

export default TeamOrg;
