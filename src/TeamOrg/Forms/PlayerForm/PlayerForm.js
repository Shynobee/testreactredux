import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import classNames from 'classnames';
import SaveIcon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class PlayerForm extends Component {
  constructor() {
    super()
    this.state = {
      playerFirstName: '',
      playerLastName: '',
      playerSport: '',
    }
  }

  handleChange (event) {
    this.setState( [event.target.name] = event.target.value );
  }

  handleClick (event) {
    this.handleChange()
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <TextField
          name='playerFirstName'
          label="Player First Name"
          placeholder="First Name"
          className={classes.textField}
          margin="normal"
          onChange={event => this.handleChange(event)}
        />
        <TextField
          name='playerLastName'
          label="Player Last Name"
          placeholder="Last Name"
          className={classes.textField}
          margin="normal"
          onChange={event => this.handleChange(event)}
        />
        <TextField
          name='playerSport'
          label="Player Sport"
          placeholder="Sport"
          className={classes.textField}
          margin="normal"
          onChange={event => this.handleChange(event)}
        />
        <Button variant="contained" size="small" className={classes.button}>
          <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
          Save
        </Button>
      </div>
    );
  }
}

PlayerForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

const PlayerFormWithStyle = withStyles(styles)(PlayerForm);

export default PlayerFormWithStyle;