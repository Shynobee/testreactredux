import React, { Component } from 'react';
import TeamOrg from './TeamOrg/TeamOrg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <TeamOrg />
      </div>
    );
  }
}

export default App;
